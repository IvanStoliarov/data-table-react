import React, { Component } from 'react'

class Button extends Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        if (this.props.handleClick) {
            this.props.handleClick(this)
        }
    }
    render() {
        return (
            <button className={this.props.className} data-role={this.props.dataRole} type={this.props.type} onClick={this.handleClick}>{this.props.value}</button>
        )
    }
}

export default Button