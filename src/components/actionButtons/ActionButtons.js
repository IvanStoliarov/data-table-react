import React, { Component } from "react";
import Button from '../button/Button'

class ActionButtons extends Component {
    constructor() {
        super();
        this.addRowHandleClick = this.addRowHandleClick.bind(this)
    }

    addRowHandleClick(component) {
        this.props.handleAddClick(component.props.dataRole);
    }


    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='actionButtons'>
                        <Button dataRole='addRow' handleClick={this.addRowHandleClick} value='Add new row' />
                        <Button handleClick={this.props.handleDeleteClick} value='Delete row' />
                        <Button dataRole='showData' handleClick={this.addRowHandleClick} value='Demo data' />
                        <Button handleClick={this.props.handleClearClick} value='Clear table' />
                        <Button dataRole='importData' handleClick={this.addRowHandleClick} value='Export table' />
                    </div>
                </div>
            </div>
        )
    }
}

export default ActionButtons;
