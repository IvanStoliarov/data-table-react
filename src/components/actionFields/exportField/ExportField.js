import React, { Component } from "react";

class ExportField extends Component {
    render() {
        const JSONdata = JSON.stringify(this.props.data)

        return (
            <div>{JSONdata}</div>
        )
    }
}

export default ExportField