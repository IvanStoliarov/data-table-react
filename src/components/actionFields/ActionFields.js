import React, { Component } from "react";
import AddField from './addField/AddField'
import ExportField from './exportField/ExportField'
import ImportData from "./importData/ImportData";

class ActionFields extends Component {
    render() {
        if (this.props.activeField === 'addRow') {
            return (
                <AddField handleSubmit={this.props.handleSubmit} />
            )
        } else if (this.props.activeField === 'showData') {
            return <ExportField data={this.props.data} />
        } else if (this.props.activeField === 'importData') {
            return <ImportData importData={this.props.importData} />
        } else {
            return false
        }
    }
}

export default ActionFields;
