import React, { Component } from 'react'
import Form from '../../form/Form'
import InputTextarea from '../../form/input/InputTextarea'
import Button from '../../button/Button'

class ImportData extends Component {
    constructor() {
        super()
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = ({ data: '' })
    }

    handleChange(event) {
        this.setState({ data: event.target.value })
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = this.state.data,
         dataToImport = JSON.parse(data);
        this.props.importData(dataToImport);
    }
    render() {

        return (
            <Form onSubmit={this.handleSubmit}>
                <InputTextarea className='w-100' handleChange={this.handleChange} value={this.state.data} />
                <Button type='submit' value='import' />
            </Form>
        )
    }
}

export default ImportData