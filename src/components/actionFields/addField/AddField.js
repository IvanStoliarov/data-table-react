import React, {Component} from "react";
import Input from '../../form/input/Input'
import InputCheckbox from '../../form/input/InputCheckbox'
import Form from '../../form/Form'
import Button from '../../button/Button'

class AddField extends Component {
    constructor() {
        super();
        this.initialState = {name: '', qty: '', availability: false};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOnchange = this.handleOnchange.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.state = this.initialState;
    }

    handleOnchange(event) {
        const data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    handleCheckboxChange(value) {
        this.setState({availability: value})
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.handleSubmit(this.state);
        this.setState(this.initialState);
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <Input labelTarget='name'
                           labelValue='Name'
                           handleOnchange={this.handleOnchange}
                           required={true}
                           value={this.state.name}
                           id='name'
                           name='name'/>
                </div>
                <div className="form-group">
                    <Input labelTarget='qty'
                           labelValue='Qty'
                           handleOnchange={this.handleOnchange}
                           required={true}
                           value={this.state.qty}
                           id='qty'
                           type='number'
                           min='0'
                           name='qty'/>
                </div>
                <div className="form-check">
                    <InputCheckbox
                        handleCheckboxChange={this.handleCheckboxChange}
                        checked={this.state.availability}
                        name='availability'
                        id='availability'
                        labelTarget='availability'
                        labelValue='Availability'
                    />
                </div>
                <Button type='submit' value='Add row'/>
            </Form>
        )
    }
}

export default AddField;
