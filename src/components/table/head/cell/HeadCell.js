import React, { Component } from "react";

class HeadCell extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = { sorted: 0 }
    }
    handleClick() {
        if (this.props.sorted) {
            const sortVariant = this.state.sorted;
            this.props.handleClick(this.props.name, sortVariant)
            this.setState({ sorted: sortVariant ? 0 : 1 });
        } else{
            return
        }
    }


    render() {
        return (
            <th onClick={this.handleClick}>{this.props.name}</th>
        )
    }
}

export default HeadCell;