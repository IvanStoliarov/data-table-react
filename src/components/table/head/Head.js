import React, { Component } from "react";
import HeadCell from './cell/HeadCell'

class Header extends Component {

    render() {
        return (
            <tr>
                <HeadCell sorted={true} handleClick={this.props.sortData} name='id'/>
                <HeadCell sorted={true} handleClick={this.props.sortData} name='name'/>
                <HeadCell sorted={true} handleClick={this.props.sortData} name='qty'/>
                <HeadCell sorted={true} handleClick={this.props.sortData} name='availability'/>
                <HeadCell sorted={false} name='delete'/>
            </tr>
        )
    }
}

export default Header;