import React, {Component} from "react";
import Header from "./head/Head";
import Inner from "./inner/Inner";
import Paginator from '../paginator/Paginator'

class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {pageQty: '', activePage: 1};
        this.changeActivePage = this.changeActivePage.bind(this);
    }

    componentWillMount() {
        const data = this.props.data,
            pageQty = this.countPages(data);
        this.setState({data: [...data], pageQty});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.props.data) {
            const data = nextProps.data,
                pageQty = this.countPages(data);
            this.setState({data: [...data], pageQty});
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextState.activePage > nextState.pageQty) {
            this.setState({activePage: nextState.pageQty})
        }
    }

    changeActivePage(value) {
        this.setState({activePage: value.props.value})
    }

    countPages(data) {
        return Math.ceil(data.length / 7)
    }


    render() {

        return (
            <>
                <table className='table'>
                    <tbody>
                    <Header sortData={this.props.sortData}/>
                    <Inner
                        saveDraggedEl={this.props.saveDraggedEl}
                        insertDraggedEl={this.props.insertDraggedEl}
                        onHoverInsert={this.props.onHoverInsert}
                        dragEnd={this.props.dragEnd}
                        isToDelete={this.props.isToDelete}
                        activePage={this.state.activePage}
                        data={this.props.data}/>
                    </tbody>
                </table>
                <Paginator activePage={this.state.activePage} qty={this.state.pageQty}
                           changeActivePage={this.changeActivePage}/>
            </>
        )
    }
}

export default Table
