import React, {Component} from "react";
import InputCheckbox from "../../../form/input/InputCheckbox";


class InnerRow extends Component {
    constructor(props) {
        super(props);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
        this.handleDragOver = this.handleDragOver.bind(this);
        this.handleDragEnd = this.handleDragEnd.bind(this);
    }

    handleDragStart(event) {
        this.props.saveDraggedEl(this.props.data, event.currentTarget);
    }

    handleDrop() {
        this.props.insertDraggedEl(this.props.data);
    }

    handleDragOver() {
        this.props.onHoverInsert(this.props.data)
    }

    handleOnChange(value) {
        this.props.isToDelete(this.props.data.id, value);
    }

    handleDragEnd(){
        this.props.dragEnd();
    }

    render() {
        return (
            <tr draggable={this.props.draggable}
                onDragOver={this.handleDragOver}
                onDragStart={this.handleDragStart}
                onDragEnd = {this.handleDragEnd}
                onDrop={this.handleDrop}
            >
                <td>{this.props.data.id}</td>
                <td>{this.props.data.name}</td>
                <td>{this.props.data.qty}</td>
                <td>{this.props.data.availability ? 'yes' : 'no'}</td>
                <td><InputCheckbox handleCheckboxChange={this.handleOnChange} checked={this.props.data.toDelete}
                                   type='checkbox'/></td>
            </tr>
        )
    }
}

export default InnerRow;
