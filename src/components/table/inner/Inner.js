import React, { Component } from "react";
import InnerRow from './row/InnerRow'

class Inner extends Component {
    constructor(props) {
        super(props);
        this.state = { data: this.props.data, activePage: this.props.activePage }
    }

    componentWillMount() {
        const dataToShow = this.separateData(this.state.data, this.state.activePage);
        this.setState({ dataToShow: dataToShow });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.props.data) {
            this.setState({ data: nextProps.data });
        } else if (nextProps.activePage !== this.state.activePage) {
            this.setState({ activePage: nextProps.activePage });
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextState.activePage !== this.state.activePage || nextState.data !== this.state.data) {
            const dataToShow = this.separateData(nextState.data, nextState.activePage);
            this.setState({ dataToShow: dataToShow });
        }
    }


    separateData(data, pageNum) {
        const rowsPerPage = 7;

        return [...data].splice((pageNum - 1) * rowsPerPage, rowsPerPage);
    }


    render() {

        return (
            this.state.dataToShow.map((dataItem, i) =>
                <InnerRow draggable='true'
                    saveDraggedEl={this.props.saveDraggedEl}
                    insertDraggedEl={this.props.insertDraggedEl}
                    onHoverInsert={this.props.onHoverInsert}
                    dragEnd={this.props.dragEnd}
                    isToDelete={this.props.isToDelete}
                    key={i}
                    data={dataItem} />
            )
        )
    }
}

export default Inner;
