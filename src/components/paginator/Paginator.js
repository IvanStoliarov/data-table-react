import React, { Component } from "react";
import Button from '../button/Button'

class Paginator extends Component {
    render() {
        if (this.props.qty > 1) {
            const buttons = [];
            for (let index = 1; index <= this.props.qty; index++) {
                buttons.push(index)
            }

            return (
                <div className='pagination p-2'>
                    {buttons.map((button, i) =>
                        <li className={`page-item ${button === this.props.activePage ? 'active' : ''}`} key={i}>
                            <Button handleClick={this.props.changeActivePage}
                                className={'page-link'}
                                // className={`btn ${button === this.props.activePage ? 'btn-outline-primary' : 'btn-outline-dark'}`}
                                value={button}  />
                        </li>
                    )}
                </div>
            )
        } else return false

    }
}

export default Paginator
