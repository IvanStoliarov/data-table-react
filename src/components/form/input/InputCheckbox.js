import React, {Component} from 'react'

class InputCheckbox extends Component {
    constructor(){
        super();
        this.handleOnchange= this.handleOnchange.bind(this);
    }

    handleOnchange(event){
        this.props.handleCheckboxChange(event.target.checked, event.target.name)
    }
    render() {
        return (
            <>
                <input onChange={this.handleOnchange}
                       checked={this.props.checked ? 'checked' : ''}
                       type='checkbox'
                       name={this.props.name}
                       id={this.props.id}/>
                <label htmlFor={this.props.labelTarget}>{this.props.labelValue}</label>
            </>
        )
    }
}

export default InputCheckbox;
