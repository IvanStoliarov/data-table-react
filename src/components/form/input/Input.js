import React, { Component } from 'react'

class Input extends Component {
    render() {
        return (
            <>
                <label htmlFor={this.props.labelTarget}>{this.props.labelValue}</label>
                <input type={this.props.type} min={this.props.min} onChange={this.props.handleOnchange} required={this.props.required} className="form-control" value={this.props.value} id={this.props.id} name={this.props.name}></input>
            </>
        )
    }
}
export default Input;