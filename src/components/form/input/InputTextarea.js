import React, { Component } from 'react'

class InputTextarea extends Component{
    render(){
        return(
            <textarea className={this.props.className} onChange={this.props.handleChange} value={this.props.value}></textarea>
        )
    }
}

export default InputTextarea