import React, {Component} from "react";
import './App.scss'
import ActionButtons from './components/actionButtons/ActionButtons'
import ActionFields from './components/actionFields/ActionFields'
import Input from './components/form/input/Input'
import Table from './components/table/Table'
import data from './data'

class App extends Component {
    constructor() {
        super();
        this.state = {
            data: data,
            dataToShow: []
        };
        this.sortData = this.sortData.bind(this);
        this.showAddField = this.showAddField.bind(this);
        this.addRow = this.addRow.bind(this);
        this.deleteRows = this.deleteRows.bind(this);
        this.isToDelete = this.isToDelete.bind(this);
        this.clearTable = this.clearTable.bind(this);
        this.importData = this.importData.bind(this);
        this.saveDraggedEl = this.saveDraggedEl.bind(this);
        this.insertDraggedEl = this.insertDraggedEl.bind(this);
        this.onHoverInsert = this.onHoverInsert.bind(this);
        this.dragEnd = this.dragEnd.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount() {
        this.setState({dataToShow: [...this.state.data]})
    }

    handleSearch(event) {
        const searchValue = event.target.value,
            initialData = this.state.data;

        function searchResult(element) {
            const dataValue = element.name,
                index = dataValue.search(searchValue);
            if (index >= 0) {
                return element;
            }
        }

        const newData = initialData.filter(searchResult);
        this.setState({dataToShow: newData})

    }

    saveDraggedEl(elData, domEl) {
        this.draggedEl = elData;
        this.setState({data: this.state.dataToShow})
    }

    insertDraggedEl(elData) {

        const data = [...this.state.dataToShow], draggedEl = this.draggedEl;

        function isInsertedIdEqual(element) {
            return element.id === elData.id;
        }

        function isDraggedIdEqual(element) {
            return element.id === draggedEl.id;
        }

        const insertedElCount = data.findIndex(isInsertedIdEqual),
            draggedElCount = data.findIndex(isDraggedIdEqual);
        if (draggedElCount < insertedElCount) {
            data.splice(insertedElCount + 1, 0, this.draggedEl);
            data.splice(draggedElCount, 1);
        } else {
            data.splice(insertedElCount + 1, 0, this.draggedEl);
            data.splice(draggedElCount + 1, 1);
        }
        this.setState({data: data});
        this.setState({dataToShow: data});
    }

    onHoverInsert(elementData) {
        this.setState({dataToShow: [...this.state.data]});
        this.insertDraggedEl(elementData);
    }

    dragEnd() {


    }

    sortData(value, sortVariant) {
        const data = [...this.state.data];
        sortVariant ?
            data.sort((a, b) => (a[value] > b[value]) ? 1 : -1) :
            data.sort((a, b) => (a[value] < b[value]) ? 1 : -1);
        this.setState({data: data});
        this.setState({dataToShow: data});
    }

    showAddField(value) {
        this.setState({activeField: value})
    }

    importData(value) {
        this.setState({data: value});
        this.setState({dataToShow: value});
    }

    addRow(data) {
        const prevData = [...this.state.data];
        if (prevData.length) {
            data.id = Math.max.apply(Math, prevData.map(function (o) {
                return o.id;
            })) + 1;
        } else {
            data.id = 0;
        }
        prevData.push(data);
        this.setState({data: prevData});
        this.setState({dataToShow: prevData})
    }

    isToDelete(id, value) {
        const data = [...this.state.data];
        data.map((element) => {
            if (element.id === id) {
                element.toDelete = value;
            }
        });
        this.setState({data: data});
        this.setState({dataToShow: data});
    }


    deleteRows() {
        const data = [...this.state.data],
            dataToShow = [...this.state.dataToShow]

        function isWillBeDeleted(element) {
            return !element.toDelete;
        }

        const newData = data.filter(isWillBeDeleted),
            newDatatoShow = dataToShow.filter(isWillBeDeleted);

        this.setState({dataToShow: newDatatoShow});
        this.setState({data: newData});
    }

    clearTable() {
        this.setState({dataToShow: []});
        this.setState({data: []});
    }

    render() {

        return (
            <div className='container'>
                <div className="App">
                    <div className='row'>
                        <div className="col mb-4">
                            <Input labelValue='SEARCH BY NAME' id='search' name={'search'} labelTarget={'search'}
                                   type='text' handleOnchange={this.handleSearch}/>
                        </div>
                    </div>
                    <Table sortData={this.sortData}
                           saveDraggedEl={this.saveDraggedEl}
                           insertDraggedEl={this.insertDraggedEl}
                           onHoverInsert={this.onHoverInsert}
                           dragEnd={this.dragEnd}
                           isToDelete={this.isToDelete}
                           data={this.state.dataToShow}/>

                    <ActionButtons handleClearClick={this.clearTable} handleDeleteClick={this.deleteRows}
                                   handleAddClick={this.showAddField}/>
                    <ActionFields data={this.state.data} importData={this.importData} handleSubmit={this.addRow}
                                  activeField={this.state.activeField}/>
                </div>
            </div>
        );
    }
}

export default App;
