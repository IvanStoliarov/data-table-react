const data = [
    {id: 0, name: 'zero', qty: 1, availability: true},
    {id: 1, name: 'one', qty: 2, availability: false},
    {id: 2, name: 'two', qty: 3, availability: false},
    {id: 3, name: 'three', qty: 4, availability: false},
    {id: 4, name: 'one', qty: 2, availability: false},
    {id: 5, name: 'two', qty: 3, availability: false},
    {id: 6, name: 'three', qty: 4, availability: false},
    {id: 7, name: 'one', qty: 2, availability: false},
    {id: 8, name: 'two', qty: 3, availability: false},
    {id: 9, name: 'three', qty: 4, availability: false},
    {id: 10, name: 'one', qty: 2, availability: false},
    {id: 11, name: 'two', qty: 3, availability: false},
    {id: 12, name: 'three', qty: 4, availability: false},
];

export default data;
